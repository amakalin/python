# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 13:35:46 2018

@author: amakalinao1187
"""
from pm_utilities.redshift_utilities import transfer_class
from pm_utilities import general_utilities

#import os
#import pandas


#def get_data():
##    def image(cat):
##        cat_dict = {
##            "Shell": "Facilities",
##            "Conveyance": "Conveyance",
##            "Electrical": "Electrical",
##            "Equipment": "Equipment",
##            "FPLS": "FPLS",
##            "HVAC": "hvac",
##            "Interiors": "Structures",
##            "Plumbing": "Plumbing",
##            "Site": "Inventory",
##        }
##        name = cat_dict.get(cat, "Track")
##        return f"http://ctaweb/sites/TAM/SiteAssets/Icon%20Photos/{name}%20Icon.png"
#
#    os.chdir(os.path.dirname(os.path.abspath(__file__)))
#
#    df_header = pandas.read_csv('Facilities Framework.xlsx', sheet_name='stations', nrows=0)
#    df = pandas.read_excel('Facilities Framework.xlsx', sheet_name='stations', skiprows=1)
#    df = df[~pandas.isnull(df['Line'])]
#
#    column_headers = {}
#    last_real_col = None
#    for col_header, col in zip(df_header.columns, df.columns):
#        if 'Unnamed: ' not in col_header:
#            last_real_col = col_header
#        column_headers[col] = last_real_col
#
#    final_df = []
#    base_columns = ['Line', 'Branch', 'Station', 'Code', 'Infor Code', 'Date?']
#    other_columns = df.columns.tolist()[len(base_columns):]
#    for i, row in df.iterrows():
#        for col in other_columns:
#            final_df.append(row[base_columns].tolist() + [column_headers[col], col, row[col]])
#    final_df = pandas.DataFrame(final_df, columns=base_columns + ['category', 'score_name', 'score_value'])
#
#    df2 = final_df[-final_df['score_name'].isin(['Facility Score', 'Shell Score', 'Interiors Score', 'Plumbing Score', 'Electrical Score', 'Conveyance Score', 'HVAC Score', 'FPLS Score', 'Site Score', 'Equipment Score', 'Structure Score'])]
#    df2 = df2.replace("*", " ")
#
#    df2['image_url'] = df2['category'].apply(image)
#
#    df2.to_excel('rail_stations.xlsx', index=False)


def load_data():
    shared_folder_name = general_utilities.get_shared_directory_path()
    transfer_class.kill_previous_connections(["am_cost_inventory"])
    transfer_class.transfer(
        transfer_class.Download(pathname=f'{shared_folder_name}/Aloysius/Redshift Uploads/Cost Model Inventory.csv'),
        table_name='am_cost_inventory',
        s3_file_name='am_cost_inventory',
        cleanup_options={'complete_refresh': True},
        columns={'score_value': 'float'},
        admin_options={
            'category': 'Website',
            'scheduled': '3:00 AM',
        }
    ).execute(user="amakalinao")


def main():
#    get_data()
    load_data()


if __name__ == '__main__':
    main()
