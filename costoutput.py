# -*- coding: utf-8 -*-
"""
Created on Mon Dec 17 13:51:40 2018

@author: amakalinao1187
"""

from pm_utilities.redshift_utilities import transfer_class
from pm_utilities import general_utilities


def load_data():
    shared_folder_name = general_utilities.get_shared_directory_path()
    #transfer_class.kill_previous_connections(["am_cost_output"], user="amakalinao")
    transfer_class.transfer(
        transfer_class.Download(pathname=f'{shared_folder_name}/Aloysius/Redshift Uploads/Cost Model Raw Output.csv'),
        table_name='am_cost_output',
        s3_file_name='am_cost_output',
        cleanup_options={'complete_refresh': True},
        columns={'score_value': 'float'},
        admin_options={
            'category': 'Website',
            'scheduled': '3:00 AM',
        }
    ).execute(user="amakalinao")


def main():
#    get_data()
    load_data()


if __name__ == '__main__':
    main()
