# -*- coding: utf-8 -*-
"""
Created on Tue Dec  4 15:33:33 2018

@author: amakalinao1187
"""

#Cleaning the COST Output
import pandas as pd

#read excel file
loc = ("C:\\Users\\AMakalinao1187\\Documents\\COST Model Outputs\\COSTModelOutput.csv")
sheet = pd.read_csv(loc, encoding = "ISO-8859-1")
rawData = sheet

#Step 1: Load up the data into 4 different sections
#1. Investment cost by year
#2. Condition by year
#3. Backlog cost by year
#4. Actions by year

assetType = sheet.iloc[:,15]

#test so I know what's going on
test = sheet.iloc[:,91]
testFrame = pd.DataFrame(assetType)
testFrame['test'] = pd.Series(test, index=testFrame.index)

#Run 4 for loops to load certain aspects of the input to DataFrames for future tidying
invest = pd.DataFrame(assetType)
for x in range(21):
    y = x
    x = x + 2018 #name of column
    y = y + 90 #index of sheet
    fill = sheet.iloc[:,y]
    x = str(x)
    invest[x] = pd.Series(fill, index=invest.index) #appends Series/column to DataFrame
    x = int(x)


condition = pd.DataFrame(assetType)
for x in range(22):
    y = x
    x = x + 2017
    y = y + 111
    fill = sheet.iloc[:,y]
    x = str(x)
    condition[x] = pd.Series(fill, index=invest.index)
    x = int(x)   
    
backlog = pd.DataFrame(assetType)
for x in range(21):
    y = x
    x = x + 2018
    y = y + 133
    fill = sheet.iloc[:,y]
    x = str(x)
    backlog[x] = pd.Series(fill, index=invest.index)
    x = int(x)  

action = pd.DataFrame(assetType)
for x in range(21):
    y = x
    x = x + 2018
    y = y + 155
    fill = sheet.iloc[:,y]
    x = str(x)
    action[x] = pd.Series(fill, index=invest.index)
    x = int(x)  

#______________________________________________________________________________

#Step 2: Create the following weighted sections
#1. % Below ULB
#2. Weighted Age
#3. % Below 3
#4. Weighted Condition
#5. Quantity in miles
    
quantity = sheet.iloc[:,22]
test = list();
for index in quantity: #creates a list that then changes quantity from str to int
    index = index.replace(',','')
    index = float(index)
    test.append(index)
age = sheet.iloc[:,69]
weightedAge = age*test #for calculation of average age for TAMP

startCondition = sheet.iloc[:,111]
weightedCondition = startCondition*test #calc of average cond for TAMP

three = list();
a = 0
for index in startCondition: #If the cond is under 3 add its quantity
    if index < 3:
        three.append(test[a])
        a = a + 1
    else:
        three.append(0)
        a = a + 1
underThree = pd.Series(three)

ulb = sheet.iloc[:,33]
pastIt = list(); #If past ULB add its quantity
b = 0
for index in ulb:
    if index < age[b]:
        pastIt.append(test[b])
        b = b + 1
    else:
        pastIt.append(0)
        b = b + 1
pastUlb = pd.Series(pastIt)  

#change names of columns
weightedAge = pd.DataFrame({'Weighted Age':weightedAge.values})
weightedCondition = pd.DataFrame({'Weighted Condition':weightedCondition.values})
underThree = pd.DataFrame({'% Under 3':underThree.values})
pastUlb = pd.DataFrame({'% Past ULB':pastUlb.values})

#append to raw data
rawData = rawData.join(weightedAge)
rawData = rawData.join(weightedCondition)
rawData = rawData.join(underThree)
rawData = rawData.join(pastUlb)

#______________________________________________________________________________

#Step 3: Use the equivalent of R tidyr and gather to transform the data = melt

#tidies the data by collapsing columns for PowerBi --> year and value
#everything is a string so turn them to numbers
tidy_invest = pd.melt(invest, id_vars='RTAAssetType', var_name='year', value_name='investment')
tidy_invest[["year"]] = tidy_invest[["year"]].apply(pd.to_numeric)
tidy_invest[["investment"]] = tidy_invest[['investment']].replace('[\$,]','',regex=True).astype(float)

tidy_condition = pd.melt(condition, id_vars='RTAAssetType', var_name='year', value_name='condition')
tidy_condition[["year", "condition"]] = tidy_condition[["year","condition"]].apply(pd.to_numeric)

tidy_backlog = pd.melt(backlog, id_vars='RTAAssetType', var_name='year', value_name='backlog')
tidy_backlog[["year"]] = tidy_backlog[["year"]].apply(pd.to_numeric)
tidy_backlog[["backlog"]] = tidy_backlog[['backlog']].replace('[\$,]','',regex=True).astype(float)


tidy_action = pd.melt(action, id_vars='RTAAssetType', var_name='year', value_name='action')
tidy_action[["year"]] = tidy_action[["year"]].apply(pd.to_numeric)

#______________________________________________________________________________

#Step 4: Load up into an xlsx with different tabs, in addition to the raw data

#Load up into an excel with several sheets
writer = pd.ExcelWriter('COST Model Raw Output.xlsx')
rawData.to_excel(writer, 'rawData')
tidy_invest.to_excel(writer, 'invest')
tidy_condition.to_excel(writer, 'cond')
tidy_backlog.to_excel(writer, 'backlog')
tidy_action.to_excel(writer, 'action')
writer.save()
