# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 10:56:35 2018

@author: amakalinao1187
"""

import pandas
#import csv


def get_data():
    base_url = "http://10.120.120.108/pdt/pdtlist.php?g=Signal&dg=&z=&dr=&l=&n=&gto=&tf=&skip={}"
    ret = []
    skip = 0
    while True:
        print(skip)
        df = pandas.read_html(base_url.format(skip), header=0)[0]
        if df.shape[0] == 0:
           break
        else:
            ret.append(df)
            skip += 500
    df = pandas.concat(ret, ignore_index=True)
    df.to_csv('test_Matt.csv', index=False)


if __name__ == '__main__':
    get_data()
